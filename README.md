# Présentation

Bienvenue dans ce dépôt qui centralise les connaissances, ainsi que diverses informations concernant l'infrastructure, les logiciels et les applications développés et/ou utilisés par l'AEE.

Ce dépôt sert à regrouper des informations importantes, mais pour éviter tout doublon et garantir l'accès aux dernières mises à jour, nous vous recommandons fortement de consulter la documentation officielle disponible sur notre wiki : [https://wiki.bde-enssat.fr](https://wiki.bde-enssat.fr).

N'hésitez pas à vous référer au wiki pour des informations complètes et à jour sur nos projets et notre infrastructure.

# Le wiki

[https://wiki.bde-enssat.fr](https://wiki.bde-enssat.fr)

Il est modifiable par toutes les personnes possédant un compte sur le domaine `*@bde-enssat.fr`